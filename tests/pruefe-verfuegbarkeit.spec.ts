import { test, expect, type Page } from '@playwright/test';

test.beforeEach(async ({ page }) => {
    await page.goto('https://www.nintendo.de/');

    // Datenschutzbestimmungen
    await page.locator('.plo-overlay__body-bottom >> nth=0').click();

    // open nintendo store
    await page.locator('div[role="button"]:has-text("My Nintendo Store")').click();



});


test.describe('Prüfe Verfügbarkeit in Kategory Zubehör', () => {

    test.beforeEach(async ({ page }) => {
        // Kategory Zubehör
        await page.waitForSelector('#scndlvl_store_Accessories')
        await page.click('#scndlvl_store_Accessories')
    });

    test('Nintendo Switch-Station weiß sollte ausverkauft sein', async ({ page }) => {

        await oeffneArtikel(page, 'Nintendo Switch-Station (mit LAN-Anschluss) Weiß');

        await pruefeBestandNichtVerfuegbar(page);
    });

    test('Nintendo Switch-Station schwarz sollte ausverkauft sein', async ({ page }) => {

        await oeffneArtikel(page, 'Nintendo Switch-Station (mit LAN-Anschluss) Schwarz');

        await pruefeBestandNichtVerfuegbar(page);
    });

    test('Super Nintendo Entertainment System™-Controller sollte ausverkauft sein', async ({ page }) => {

        await oeffneArtikel(page, 'Super Nintendo Entertainment System™-Controller');

        await pruefeBestandNichtVerfuegbar(page);
    });
});

async function oeffneArtikel(page: Page, artikelbezeichnung: string) {
    await page.waitForSelector('.searchresult_row a[title="' + artikelbezeichnung + '"] .img-responsive')
    await page.locator('.searchresult_row a[title="' + artikelbezeichnung + '"] .img-responsive').click();
}

async function pruefeBestandNichtVerfuegbar(page: Page) {

    await page.waitForLoadState();

    await (expect(page.locator('.product-add-form > #product_addtocart_form > .requires-coupon'))).toHaveText(['Derzeit nicht verfügbar']);
}